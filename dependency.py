import matplotlib.pyplot as plt
import numpy as np

data = np.array([(1,2), (2.5, 7), (3, 9), (2.8, 7), (1.4, 4), (3.5, 7.5), (4, 9), (3.2, 6.8)])
data_b = np.array([(1,2), (2.5, 7), (3, 9), (2.8, 7), (1.4, 4), (3.5, 7.5), (4, -9), (3.2, 6.8)])

def r(points, xaxis, yaxis):
    sqsum_x = 0
    sqsum_y = 0
    numerator = 0
    
    for i in points:
        sqsum_x += (i[0] - np.mean(xaxis))**2
        sqsum_y += (i[1] - np.mean(yaxis))**2
       
    for i in points:
        numerator += (i[0] - np.mean(xaxis))*(i[1] - np.mean(yaxis))
        
    denominator = (sqsum_x**0.5)*(sqsum_y**0.5)
    
    return  numerator/denominator

def comp_a(points, x_data, y_data):
    numerator = 0
    denominator = 0
    
    for i in points:
        numerator += (i[1]-y_data.mean())*(i[0]-x_data.mean())
        denominator += (i[0]-x_data.mean())**2
        
    return numerator/denominator

x, y = data.T
x_b, y_b = data_b.T

correlation = r(data, x, y)
correlation_b = r(data_b, x_b, y_b)

a = comp_a(data, x, y)
b = (y.sum() - a*x.sum())/x.size
#print(a, b)

a_b = comp_a(data_b, x_b, y_b)
b_b = (y_b.sum() - a_b*x_b.sum())/x_b.size

regression_line = [(a*i)+b for i in x]
regression_line_b = [(a_b*i)+b_b for i in x_b]

print("R equals", correlation)
print("R for new data set equals", correlation_b)


plt.scatter(x, y)
plt.plot(x, regression_line, '-r')
plt.plot(x_b, regression_line_b, '-g')
plt.axis('equal')
plt.show()