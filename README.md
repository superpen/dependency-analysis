# Dependency analysis #

This repository was created to display my solution for the task from the course DataMining at Univesity Bielefeld (https://ekvv.uni-bielefeld.de/sinfo/publ/modul/26787742)

## Task ##
The task was to examine the data set {(1,2), (2.5, 7), (3, 9), (2.8, 7), (1.4, 4), (3.5, 7.5), (4, 9), (3.2, 6.8)}.

* How big is 𝑟?
* Which are the optimal parameters 𝑎 and 𝑏 ?
* How big is the error ?
* How does a measuring error "(4,-9) instead of (4,9)" influence 𝑟?
* Plot the data set and compare the regression line.

### Prerequisites ###

python3, numpy
